# vSphere with Tanzu Notes

### Deploying a Workload Cluster
1. Get logged into vSphere w/ Tanzu enabled.  Then create a namespace.  `my-wcp` in this example
1. Download the CLI plugin for vSphere from the GUI and place in your shell's `$PATH`
1. From your CLI, log into vSphere.  Such as:
```
# This var can be used if desired
export KUBECTL_VSPHERE_PASSWORD=password
kubectl vsphere login --server=https://myserver.com:6443 --vsphere-username administrator@vsphere.local --insecure-skip-tls-verify
```
1. Set your context with `kubectl config use-context my-wcp`
1. Deploy your cluster with
```
kubectl apply -f cluster.yaml
```
1. After deploy, you can check it with
```
kubectl get tanzukubernetesclusters
# Shortened version
kubectl get tkc
```

#### Helpful commands for determining YAML values
```
# Get the Tanzu Kubernetes Releases available
kubectl get tkr
# Get VM Class Bindings, make sure these are assigned to the Namespace in the vSphere GUI
kubectl get virtualmachineclassbindings
# Storage info
kubectl describe storageclasses
```

### Deploy Sample Workload to the Cluster
First log into the Workload cluster
```
k vsphere login --server=https://vc01cl01-wcp.h2o-4-1395.h2o.vmware.com:6443 --insecure-skip-tls-verify --vsphere-username administrator@vsphere.local --tanzu-kubernetes-cluster-name tkgs-cluster-1 --tanzu-kubernetes-cluster-namespace my-wcp
```

Deploy Kuard
```
kubectl run --restart=Never --image=gcr.io/kuar-demo/kuard-amd64:blue kuard
kubectl port-forward kuard 8080:8080
```

You can use [Octant](https://octant.dev/) to view resources on the cluster.  Run with `octant`.

### Deploying more complex Workloads

First, make sure [Pod Security Policies](https://docs.vmware.com/en/VMware-vSphere/7.0/vmware-vsphere-with-tanzu/GUID-4CCDBB85-2770-4FB8-BF0E-5146B45C9543.html) are set up.

```
kubectl apply -f psp.yaml
```

Deploy nginx with a load balancer

```
kubectl apply -f nginx-lbsvc.yaml
```

Get the external IP and confirm it works in the browser.

```
kubectl get svc,pod
NAME                  TYPE           CLUSTER-IP       EXTERNAL-IP    PORT(S)        AGE
service/kubernetes    ClusterIP      10.96.0.1        <none>         443/TCP        4d21h
service/srvclb-ngnx   LoadBalancer   10.101.119.183   10.220.70.38   80:30431/TCP   67s
service/supervisor    ClusterIP      None             <none>         6443/TCP       4d21h

NAME                                READY   STATUS    RESTARTS   AGE
pod/loadbalancer-5554b69d95-69dd2   1/1     Running   0          67s
pod/loadbalancer-5554b69d95-9fqs5   1/1     Running   0          67s
```


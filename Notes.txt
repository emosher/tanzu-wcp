# Create the cluster pre-demo 
kubectl apply -f cluster-1.yaml

# Create PSP
kubectl apply -f psp.yaml

# Check status of the cluster
kubectl get tanzukubernetesclusters

# Get contexts
kubectl config get-contexts 

# Set context 
kubectl config use-context moshere

### Demo commands ###

# Login to the supervisor cluster
kubectl vsphere login --server=https://vc01cl01-wcp.h2o-4-22382.h2o.vmware.com:6443 --vsphere-username administrator@vsphere.local --insecure-skip-tls-verify

# Create the cluster
kubectl apply -f cluster-2.yaml

# Login to the workload cluster
kubectl vsphere login --server=https://vc01cl01-wcp.h2o-4-22382.h2o.vmware.com:6443 --vsphere-username administrator@vsphere.local --insecure-skip-tls-verify --tanzu-kubernetes-cluster-name tanzu-cluster-1 --tanzu-kubernetes-cluster-namespace tanzu-demo

# Set the context
kubectl config use-context tanzu-cluster-1

# Test the connection
kubectl get nodes

# Deploy demo app
kubectl apply -f nifi.yaml

# Get status of app
kubectl get all -n nifi

# Open app in browser at <IP>/nifi


#!/bin/bash
trap 'read -p "run: $BASH_COMMAND"' DEBUG

# Login to the supervisor cluster
kubectl vsphere login --server=https://vc01cl01-wcp.h2o-4-22382.h2o.vmware.com:6443 --vsphere-username administrator@vsphere.local --insecure-skip-tls-verify

kubectl config use-context vc01cl01-wcp.h2o-4-22382.h2o.vmware.com

# Create the cluster
kubectl apply -f cluster-2.yaml

# Login to the workload cluster
kubectl vsphere login --server=https://vc01cl01-wcp.h2o-4-22382.h2o.vmware.com:6443 --vsphere-username administrator@vsphere.local --insecure-skip-tls-verify --tanzu-kubernetes-cluster-name tanzu-cluster-1 --tanzu-kubernetes-cluster-namespace tanzu-demo

# Set the context
kubectl config use-context tanzu-cluster-1

# Deploy Nginx
kubectl apply -f nginx-lbsvc.yaml